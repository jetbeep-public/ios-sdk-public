//
//  ConsoleManager.swift
//  PicPic
//
//  Created by Ruslan on 2/16/17.
//  Copyright © 2017 Ruslan Shevtsov. All rights reserved.
//

import UIKit

protocol ConsoleProtocol {
    func consoleWasUpdated(with history: String)
}

class ConsoleManager: NSObject {

    private var consoleHistory: Array<String> = []
    
    var delegate: ConsoleProtocol? = nil
    
    static let shared : ConsoleManager = {
        let instance = ConsoleManager()
        return instance
    }()
    
    func addString(new string: String) {
        consoleHistory.insert("*****", at: 0)
        consoleHistory.insert(string, at: 0)
        if let del = delegate {
            del.consoleWasUpdated(with: getInfo())
        }
    }
    
    private func getInfo() -> String {
        var infoString: String = ""
        
        for (_, historyString) in consoleHistory.enumerated() {
            infoString += historyString
            infoString += "\n"
        }
        
        return infoString
    }
}
