//
//  SettingsViewController.swift
//  PicPic
//
//  Created by Ruslan on 2/16/17.
//  Copyright © 2017 Ruslan Shevtsov. All rights reserved.
//

import UIKit
import JetBeepFramework

class SettingsViewController: UITableViewController, UITextFieldDelegate {

    @IBOutlet weak var idTextField: UITextField!
    
    @IBOutlet weak var debugModeSwitch: UISwitch!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Настройки"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let settingsManager: SettingsManager = SettingsManager()
        debugModeSwitch.isOn = settingsManager.debugMode
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }

    // MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        idTextField.resignFirstResponder()
    }
    
    // MARK: - IBActions
    
    @IBAction func changeDebugModeAction(_ sender: Any) {
        SettingsManager().debugMode = debugModeSwitch.isOn
    }
    

    // MARK: - UITextFieldDelegate
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        SettingsManager().idString = textField.text
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
