//
//  JetBeepManager.swift
//  PicPic
//
//  Created by Ruslan on 3/13/17.
//  Copyright © 2017 Ruslan Shevtsov. All rights reserved.
//

import UIKit
import JetBeepFramework
import UserNotifications

class JetBeepManager: NSObject, JetBeepDelegate {
    
    
    let kUUIDString: String = "88D22ADC-E983-4183-BB15-191C85F887FC"
    let kRegionName: String = "TestRegion"

    var inProcess: Bool = false
    
    static let sharedInstance: JetBeepManager = JetBeepManager()
    
    let dateFormatter = DateFormatter()
    
    private override init() {
        super.init()
     
        dateFormatter.dateFormat = "HH:mm:ss"
    }
    
    func setup() {
        
        JetBeep.shared.start(with: self)
        JetBeep.shared.startScanning()
        
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options: [.alert, .sound]) { (granted, error) in
                // Enable or disable features based on authorization
            }
        }

        
    }
    
    // MARK: - JetBeepDelegate
    
    func JetBeepDidFail(with error: Error?, for shop: Shop?) {
        let message: String = "Транзакция неуспешная"
        showLocalNotification(with: message, customSound: nil)
        ConsoleManager.shared.addString(new: message)
        
        inProcess = false

    }
    
    func JetBeepValueForWriting(for shop: Shop) -> [Barcode]? {
        if let valueForWriting = SettingsManager().idString {
            let barcode: Barcode = Barcode(withValue: valueForWriting)
            return [barcode]
        }
        return nil

    }
    
    func JetBeepDidWriteValue(for shop: Shop) {
        let messageForLog: String = "Транзакция успешная"
        ConsoleManager.shared.addString(new: messageForLog)
        
        let message: String = "Transaction successful"
        
        let settingsManager: SettingsManager = SettingsManager()
        if settingsManager.debugMode {
            showLocalNotification(with: messageForLog, customSound: "coins.mp3")
        } else {
            showLocalNotification(with: message, customSound: "coins.mp3")
        }
        
        inProcess = false

    }
    
    func JetBeepEnterRegion() {
        var message: String = dateFormatter.string(from: Date())
        message = message + " - Did Enter Region "
        ConsoleManager.shared.addString(new: message)
        
        let settingsManager: SettingsManager = SettingsManager()
        if settingsManager.debugMode {
            showLocalNotification(with: message, customSound: nil)
        }

    }
    
    func JetBeepDidEnterShop(for shop: Shop) {
        var message: String = dateFormatter.string(from: Date())
        message = message + " - Enter shop "
        message = message + shop.name!
        ConsoleManager.shared.addString(new: message)
    }
    
    // MARK: Helpers
    
    func showLocalNotification(with message: String, customSound: String?) {
        let notification = UILocalNotification()
        notification.alertBody = message
        notification.soundName = "Default"
        if customSound != nil {
            notification.soundName = customSound
        }
        UIApplication.shared.presentLocalNotificationNow(notification)
    }

}
