//
//  ViewController.swift
//  PicPic
//
//  Created by Ruslan on 1/17/17.
//  Copyright © 2017 Ruslan Shevtsov. All rights reserved.
//

import UIKit
import JetBeepFramework

let kShowSettingsScreenIdentifier: String = "ShowSettingsScreen"

class ViewController: UIViewController, ConsoleProtocol {
    
    @IBOutlet weak var consoleContainerView: UIView!
    @IBOutlet weak var consoleTextView: UITextView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var monitoringButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let screenSize = UIScreen.main.bounds
        let screenHeight = screenSize.height
        
        monitoringButton.isHidden = true
        
        monitoringButton.layer.cornerRadius = 3
        monitoringButton.layer.borderWidth = 1
        monitoringButton.layer.borderColor = UIColor(red: 86/255, green: 173/255, blue: 65/255, alpha: 1.0).cgColor
        monitoringButton.setTitleColor(UIColor(red: 86/255, green: 173/255, blue: 65/255, alpha: 1), for: UIControlState.normal)
        
        if screenHeight < 500 {
            backgroundImageView.image = UIImage(named: "img_bg_4.jpg")
        } else if screenHeight > 500 && screenHeight < 600 {
            backgroundImageView.image = UIImage(named: "img_bg_5.jpg")
        } else if screenHeight > 600 {
            backgroundImageView.image = UIImage(named: "img_bg_6.jpg")
        }
        
        ConsoleManager.shared.delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    // MARK - IBActions
    
    @IBAction func showSettingsAction(_ sender: Any) {
        self.performSegue(withIdentifier: kShowSettingsScreenIdentifier, sender: self)
    }
    
    // MARK: ConsoleProtocol
    
    func consoleWasUpdated(with history: String) {
        consoleTextView.text = history
    }
}

