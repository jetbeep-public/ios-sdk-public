//
//  SettingsManager.swift
//  PicPic
//
//  Created by Ruslan on 2/16/17.
//  Copyright © 2017 Ruslan Shevtsov. All rights reserved.
//

import UIKit
import JetBeepFramework


let kDistanceKey: String = "Distance"
let kCardIdKey: String = "CardId"
let kDebugModeKey: String = "DebugMode"
let kAddEndSymbolKey: String = "AddEndSymbol"

class SettingsManager: NSObject {

    var distance: Double {
        
        get {
            let defaults = UserDefaults.standard
            var distance: Double = defaults.double(forKey: kDistanceKey)
            if distance == 0 {
                distance = 0.15
                defaults.set(distance, forKey: kDistanceKey)
            }
            return distance
        }
        
        set(dis) {
            let defaults = UserDefaults.standard
            defaults.set(dis, forKey: kDistanceKey)
        }
    }
    
    var idString: String? {
        get {
            let defaults = UserDefaults.standard
            let idString: String? = defaults.string(forKey: kCardIdKey)
            return idString
        }
        
        set(idString) {
            let defaults = UserDefaults.standard
            defaults.set(idString, forKey: kCardIdKey)
        }
    }
    
    var debugMode: Bool {
        get {
            let defaults = UserDefaults.standard
            let debugMode: Bool = defaults.bool(forKey: kDebugModeKey)
            return debugMode
        }
        set(debugMode) {
            let defaults = UserDefaults.standard
            defaults.set(debugMode, forKey: kDebugModeKey)
        }
    }
    
    var addEndSymbol: Bool {
        get {
            let defaults = UserDefaults.standard
            let addEndSymbol: Bool = defaults.bool(forKey: kAddEndSymbolKey)
            return addEndSymbol
        }
        set(addEndSymbol) {
            let defaults = UserDefaults.standard
            defaults.set(addEndSymbol, forKey: kAddEndSymbolKey)
        }
    }

}
