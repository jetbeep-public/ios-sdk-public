//
//  AudioPlayer.swift
//  PicPic
//
//  Created by Ruslan on 3/3/17.
//  Copyright © 2017 Ruslan Shevtsov. All rights reserved.
//

import UIKit
import AVFoundation
import AudioToolbox

class AudioPlayer: NSObject, AVAudioPlayerDelegate {
    
//    var player: AVPlayer = AVPlayer()
    
    var player: AVAudioPlayer?
    
    func play(with soundName: String) {
        
//        if player?.isPlaying == true {
//            return
//        }
        
        guard let url = Bundle.main.url(forResource: soundName, withExtension: "mp3") else {
            print("url not found")
            return
        }
        
        do {
            /// this codes for making this app ready to takeover the device audio
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: [])
            try AVAudioSession.sharedInstance().setActive(true)
            
            /// change fileTypeHint according to the type of your audio file (you can omit this)
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3)
            
            // no need for prepareToPlay because prepareToPlay is happen automatically when calling play()
            
            player?.delegate = self
            
            if player?.prepareToPlay() == true {
                print("ready")
            }
            
            print(String(describing: player?.duration))
            
            if player!.play() == true {
                print("play")
            }
        } catch let error as NSError {
            print("error: \(error.localizedDescription)")
        }
    }
    
    // MARK: AVAudioPlayerDelegate
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        print("error")
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print("Fisnish " + String(flag))
    }
    
}
