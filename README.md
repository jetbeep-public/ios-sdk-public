JetBeep SDK version 0.5.1.

In this repository you can find a sample project with JetBeep SDK being already setup

Requirements: iOS 10.0

Integration: add JetBeepFramework.framework to project

How to work with JetBeep SDK:

-   framework starts to work in background after calling method
    public func start(with delegate: JetBeepDelegate);

-   need to subscribe for JetBeepDelegate for getting notifications;

-   need to call method 
    public func startScanning()
    for starting scanning in foreground

Class description:

-   JetBeep - it is main class of SDK.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
public class JetBeep : NSObject {

    public static let shared: JetBeep // singleton method

    public var storedShops: [Shop] { get } // property for getting shops

    public var storedOffers: [Offer] { get } // property for getting offers

    public func start(with delegate: JetBeepDelegate) // start scanning JetBeep devices

    public func startScanning() // enable readiness of connecting to JetBeep device

    public func stopScanning() // enable readiness of connecting to JetBeep device
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-   JetBeepDelegate - protocol for notifications. All methods are optional.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
public protocol JetBeepDelegate {

    public func JetBeepDidFail(with error: Error?, for shop: Shop?) // Get errors in sdk

    public func JetBeepValueForWriting(for shop: Shop) -> [Barcode]? // Ask array of Barcode for writing value

    public func JetBeepLoaded(shops: [Shop]) // Notify that shops were loaded

    public func JetBeepLoaded(offers: [Offer]) // Notify that offers were loaded

    public func JetBeepDidWriteValue(for shop: Shop) // Notify of success writing value.

    public func JetBeepEnterRegion() // Notify of entering the region with JetBeep devices

    public func JetBeepDidEnterShop(for shop: Shop) // Notify of entering the shop
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sample of working with JetBeep SDK:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class JetBeepManager: NSObject, JetBeepDelegate {

    func setup() {
        
        JetBeep.shared.start(with: self)
        JetBeep.shared.startScanning()

    }

    // MARK: - JetBeepDelegate
    
    func JetBeepValueForWriting(for shop: Shop) -> [Barcode]? {
        if let valueForWriting = SettingsManager().idString {
            let barcode: Barcode = Barcode(withValue: valueForWriting)
            return [barcode]
        }
        return nil
    
    }
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
